plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.jetbrains.kotlin.android)
}

android {
    namespace = "com.okeenea.beacons.trigger.demoapp"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.okeenea.beacons.trigger.demoapp"
        minSdk = 31
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    buildFeatures {
        viewBinding = true
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.material)
    implementation(libs.androidx.activity)
    implementation(libs.androidx.constraintlayout)

    implementation(files("../sdk/OkeeneaBeaconsTrigger.aar"))
    // Dependencies of OkeeneaBeaconsTrigger
    implementation("androidx.lifecycle:lifecycle-livedata-core-ktx:2.7.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.7.0")
    implementation("co.touchlab:stately-concurrency:1.2.3")
    implementation("com.google.android.gms:play-services-location:21.0.1")
    implementation("com.juul.kable:core:0.31.1")
    implementation("com.russhwolf:multiplatform-settings-no-arg:1.0.0")
    implementation("com.squareup.sqldelight:android-driver:1.5.5")
    implementation("com.squareup.sqldelight:runtime:1.5.5")
    implementation("dev.icerock.moko:permissions:0.12.0")
    implementation("dev.icerock.moko:resources:0.23.0")
    implementation("org.altbeacon:android-beacon-library:2.20.4")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.2")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.4.1")
}
