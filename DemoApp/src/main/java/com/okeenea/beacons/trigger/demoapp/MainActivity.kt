package com.okeenea.beacons.trigger.demoapp

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.okeenea.beacons.OkeeneaBeacon
import com.okeenea.beacons.OkeeneaBeaconsTrigger
import com.okeenea.beacons.scan
import com.okeenea.beacons.trigger
import com.okeenea.beacons.trigger.demoapp.databinding.ActivityMainBinding
import com.okeenea.beacons.trigger.demoapp.databinding.ListItemBinding
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val beaconsTrigger by lazy { OkeeneaBeaconsTrigger(applicationContext) }

    private val adapter = BeaconsListAdapter()
    private var isScanning = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()

        setContentView(binding.root)

        ViewCompat.setOnApplyWindowInsetsListener(binding.main) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        binding.beaconsList.adapter = adapter
        binding.startStopButton.setOnClickListener {
            toggleStartStop()
        }
    }

    private fun toggleStartStop() {
        if (isScanning) {
            stopScan()
        } else {
            checkPermissionsThenStartScan()
        }
    }

    private fun stopScan() {
        isScanning = false
        binding.startStopButton.setText(R.string.action_start)

        beaconsTrigger.stopScan()

        adapter.submitList(emptyList())
    }

    private fun checkPermissionsThenStartScan() {
        if (permissionsAreGranted()) {
            startScan()
        } else {
            requestPermissions()
        }
    }

    private fun startScan() {
        isScanning = true
        binding.startStopButton.setText(R.string.action_stop)
        binding.errorTextView.isVisible = false

        beaconsTrigger.startScan(onUpdate = {
            showBeacons(it)
        }, onError = {
            showError(it)
        })
    }

    private fun showError(error: Exception) {
        binding.errorTextView.text = error.message
        binding.errorTextView.apply {
            text = error.message
            isVisible = true
        }
        stopScan()
    }

    private fun showBeacons(beacons: List<OkeeneaBeacon>) {
        binding.errorTextView.isVisible = false
        adapter.submitList(beacons)
    }

    private fun permissionsAreGranted(): Boolean =
        neededPermissions.all {
            ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
        }

    private fun requestPermissions() {
        requestPermissionLauncher.launch(neededPermissions)
    }

    private val requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
        startScan()
    }

    private val neededPermissions = arrayOf(
        Manifest.permission.BLUETOOTH_SCAN,
        Manifest.permission.BLUETOOTH_CONNECT,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    private class BeaconsListAdapter : ListAdapter<OkeeneaBeacon, BeaconsListAdapter.BeaconViewHolder>(BeaconsDiffUtilItemCallback()) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeaconViewHolder =
            BeaconViewHolder(ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

        override fun onBindViewHolder(holder: BeaconViewHolder, position: Int) {
            holder.bind(getItem(position))
        }

        private class BeaconViewHolder(private val binding: ListItemBinding) : RecyclerView.ViewHolder(binding.root) {
            fun bind(beacon: OkeeneaBeacon) {
                binding.title.text = beacon.name
                binding.triggerButton.setOnClickListener {
                    beacon.trigger { exception ->
                        exception?.let {
                            Log.e("Trigger Error", "An error occured during trigger.", it)
                        }
                    }
                }
            }
        }
    }

    private class BeaconsDiffUtilItemCallback : DiffUtil.ItemCallback<OkeeneaBeacon>() {
        override fun areItemsTheSame(oldItem: OkeeneaBeacon, newItem: OkeeneaBeacon): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: OkeeneaBeacon, newItem: OkeeneaBeacon): Boolean =
            oldItem.name == newItem.name
    }
}
