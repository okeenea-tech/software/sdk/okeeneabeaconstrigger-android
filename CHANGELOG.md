# 1.1.0

- Add new supported beacons.

# 1.0.3

- Optimize library size.

# 1.0.2

- Fix an issue causing triggers to fail when cancelling previous trigger using coroutine extension.

# 1.0.1

- Fix a crash when stopping scan in a background thread.

# 1.0.0

- First release.
