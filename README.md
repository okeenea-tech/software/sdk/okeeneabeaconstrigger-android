# OkeeneaBeaconsTrigger

## Installation

1) Copy [`sdk/OkeeneaBeaconsTrigger.aar`](sdk/OkeeneaBeaconsTrigger.aar) file into your project and [add it as an AAR dependency](https://developer.android.com/studio/projects/android-library#psd-add-aar-jar-dependency).
2) Add the following required dependencies (the versions listed below have been tested) in your module :

```kotlin
implementation("androidx.lifecycle:lifecycle-livedata-core-ktx:2.7.0")
implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.7.0")
implementation("co.touchlab:stately-concurrency:1.2.3")
implementation("com.google.android.gms:play-services-location:21.0.1")
implementation("com.juul.kable:core:0.31.1")
implementation("com.russhwolf:multiplatform-settings-no-arg:1.0.0")
implementation("com.squareup.sqldelight:android-driver:1.5.5")
implementation("com.squareup.sqldelight:runtime:1.5.5")
implementation("dev.icerock.moko:permissions:0.12.0")
implementation("dev.icerock.moko:resources:0.23.0")
implementation("org.altbeacon:android-beacon-library:2.20.4")
implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.2")
implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.4.1")
```

## Usage

_Pre-requisite :_ the library does not manage permissions for you (it just checks them and fails if not granted), you need to ask users the following ones : `BLUETOOTH_SCAN, BLUETOOTH_CONNECT, ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION`.

**Important notice :** in order to see Okeenea beacons, check that your `AndroidManifest` **does not** [strongly assert that it doesn't derive physical location](https://developer.android.com/develop/connectivity/bluetooth/bt-permissions#assert-never-for-location). 

### Note about callbacks

The callbacks informs when communication with the beacon has been done. **It does not tell that the beacon did finish playing a message.**

You can use it to report any communication error to the user or to know that trigger did succeed.

### Demo app

You can try the SDK using the demo app.

### API

#### Usage

```kotlin
// Create the scanner
val beaconsTrigger = OkeeneaBeaconsTrigger(context)

// Start scan
beaconsTrigger.startScan(onUpdate = { beacons ->
    // Show the beacons or...
    // ...trigger one.
    beacons.first.trigger { exception ->
        // Handle trigger error
    }
}, onError = { exception ->
    // Handle scan errors
})

// Stop scan
beaconsTrigger.stopScan()

// Using coroutines extensions
scope.launch {
    beaconsTrigger.scan().catch { exception ->
        showError(exception)
    }.collectLatest { beacons ->
        // Show the beacons or...
        // ...trigger one.
        beacons.first().trigger()
    }
}
```

#### Definitions

```kotlin
class OkeeneaBeaconsTrigger(
    private val context: Context,
    callbackDispatcher: CoroutineDispatcher = Dispatchers.Main // You can customize on which context you want to receive callbacks, default is Main to use it easily from UI.
) {
    fun startScan(onUpdate: (List<OkeeneaBeacon>) -> Unit, onError: (Exception) -> Unit)
    fun stopScan()
    
    // Coroutine extension
    suspend fun scan(): Flow<List<OkeeneaBeacon>>
}

data class OkeeneaBeacon {
    val id: String
    val name: String
    val kind: OkeeneaBeaconKind
    val rssi: Int
    val rssiUpdatedAt: Instant

    fun trigger(onComplete: (Exception?) -> Unit)
    fun dispose()

    // Coroutine extension
    suspend fun trigger()
}

enum class OkeeneaBeaconKind {
    POINT_OF_INTEREST,
    AUDIBLE_PEDESTRIAN_SIGNAL;
}
```

## Copyright

Copyright © Okeenea 2025. All rights reserved.
